import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CriteriaModule } from './criteria/criteria.module';
import { GroupCriteriaController } from './group-criteria/group-criteria.controller';
import { GroupCriteriaModule } from './group-criteria/group-criteria.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/khungtieuchi'),
    CriteriaModule,
    GroupCriteriaModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
