export class CriteriaDto {
    readonly name: string;
    readonly description: string;
    readonly question: Array<string>;
    createAt: Date;
    createBy: string;
    updateAt: Date;
    updateBy: string;
    tags:Array<string>;
  }