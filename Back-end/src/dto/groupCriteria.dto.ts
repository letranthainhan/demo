export class GroupCriteriaDto {
    readonly name: string;
    readonly description: string;
    readonly idCriteria: Array<string>;
    createAt: Date;
    createBy: string;
    updateAt: Date;
    updateBy: string;
  }