import { Schema } from '@nestjs/mongoose';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { CriteriaDto } from 'src/dto/criteria.dto';
import { Criteria } from 'src/schemas/criteria.schemas';
import { CriteriaService } from './criteria.service';

@Controller('criteria')
export class CriteriaController {
  constructor(private readonly criteriaService: CriteriaService) {}
  @Get()
  async findAll(): Promise<Criteria[]> {
    return this.criteriaService.findAll();
  }
  // @Get(':id')
  // async findOne(@Param('id') id): Promise<Criteria> {
  //   return this.criteriaService.findOne(id);
  // }
  // @Get(':name')
  // async findOnename(@Param('name') name): Promise<Criteria> {
  //   return this.criteriaService.findOneName(name);
  // }

  @Post()
  create(@Body() createCriteriaDto: CriteriaDto): Promise<Criteria> {
    createCriteriaDto.createAt = this.criteriaService.At();
    createCriteriaDto.createBy = this.criteriaService.By();
    return this.criteriaService.create(createCriteriaDto);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<Criteria> {
    return this.criteriaService.delete(id);
  }
  @Post(':id')
  update(
    @Body() updateCriteriaDto: CriteriaDto,
    @Param('id') id,
  ): Promise<Criteria> {
    updateCriteriaDto.updateAt = this.criteriaService.At();
    updateCriteriaDto.updateBy = this.criteriaService.By();
    return this.criteriaService.update(id, updateCriteriaDto);
  }
  @Get('find')
  findMultiTag(@Body() tags: Array<string>): Promise<Criteria[]> {
    return this.criteriaService.findMultiTags(tags);
  }
  @Get(':tag')
  findOneTag(@Param('tag') tag): Promise<Criteria[]> {
    return this.criteriaService.findOneTags(tag);
  }
}
