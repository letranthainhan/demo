import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Criteria } from 'src/schemas/criteria.schemas';

@Injectable()
export class CriteriaService {
  constructor(
    @InjectModel('Criteria') private readonly criteriaModel: Model<Criteria>,
  ) {}
  async findAll(): Promise<Criteria[]> {
    return await this.criteriaModel.find();
  }

  async findOne(id: string): Promise<Criteria> {
    return await this.criteriaModel.findOne({ _id: id });
  }
  async findOneName(name: string): Promise<Criteria> {
    return await this.criteriaModel.findOne({ name: name });
  }
  async findOneTags(tag: string): Promise<Criteria[]> {
    return await this.criteriaModel.find({ tags: { $in: [tag] } });
  }
  async findMultiTags(tags: Array<string>): Promise<Criteria[]> {
    return await this.criteriaModel.find({ tags: { $all: tags } });
  }
  async create(criteria: Criteria): Promise<Criteria> {
    const newCriteria = new this.criteriaModel(criteria);
    return await newCriteria.save();
  }
  At(): Date {
    var newCriteria = new Date();
    return newCriteria;
  }
  By(): string {
    return 'User';
  }
  async delete(id: string): Promise<Criteria> {
    return await this.criteriaModel.findByIdAndRemove(id);
  }
  async update(id: string, criteria: Criteria): Promise<Criteria> {
    return await this.criteriaModel.findByIdAndUpdate(id, criteria, {
      new: true,
    });
  }
}
