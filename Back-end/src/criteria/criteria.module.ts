import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CriteriaSchema } from 'src/schemas/criteria.schemas';
import { CriteriaController } from './criteria.controller';
import { CriteriaService } from './criteria.service';

@Module({
  imports:[MongooseModule.forFeature([{name:'Criteria',schema:CriteriaSchema}])],
  controllers: [CriteriaController],
  providers: [CriteriaService]
})
export class CriteriaModule {}
