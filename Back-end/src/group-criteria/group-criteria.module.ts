import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GroupCriteriaSchema } from 'src/schemas/groupCriteria.schemas';
import { GroupCriteriaController } from './group-criteria.controller';
import { GroupCriteriaService } from './group-criteria.service';

@Module({
  imports:[MongooseModule.forFeature([{name:'GroupCriteria',schema:GroupCriteriaSchema}])],
  controllers: [GroupCriteriaController],
  providers: [GroupCriteriaService]
})
export class GroupCriteriaModule {}
