import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GroupCriteria } from 'src/schemas/groupCriteria.schemas';

@Injectable()
export class GroupCriteriaService {
  constructor(
    @InjectModel('GroupCriteria')
    private readonly groupCriteriaModel: Model<GroupCriteria>,
  ) {}
  async findAll(): Promise<GroupCriteria[]> {
    return await this.groupCriteriaModel.find();
  }

  async findOne(id: string): Promise<GroupCriteria> {
    return await this.groupCriteriaModel.findOne({ _id: id });
  }
  async create(GroupCriteria: GroupCriteria): Promise<GroupCriteria> {
    const newCriteria = new this.groupCriteriaModel(GroupCriteria);
    return await newCriteria.save();
  }
  At(): Date {
    var newCriteria = new Date();
    return newCriteria;
  }
  By(): string {
    return 'User';
  }
  async delete(id: string): Promise<GroupCriteria> {
    return await this.groupCriteriaModel.findByIdAndRemove(id);
  }
  async update(
    id: string,
    GroupCriteria: GroupCriteria,
  ): Promise<GroupCriteria> {
    return await this.groupCriteriaModel.findByIdAndUpdate(id, GroupCriteria, {
      new: true,
    });
  }
}
