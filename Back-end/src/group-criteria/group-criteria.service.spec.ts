import { Test, TestingModule } from '@nestjs/testing';
import { GroupCriteriaService } from './group-criteria.service';

describe('GroupCriteriaService', () => {
  let service: GroupCriteriaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GroupCriteriaService],
    }).compile();

    service = module.get<GroupCriteriaService>(GroupCriteriaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
