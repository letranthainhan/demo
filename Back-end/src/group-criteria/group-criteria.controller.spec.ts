import { Test, TestingModule } from '@nestjs/testing';
import { GroupCriteriaController } from './group-criteria.controller';

describe('GroupCriteriaController', () => {
  let controller: GroupCriteriaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupCriteriaController],
    }).compile();

    controller = module.get<GroupCriteriaController>(GroupCriteriaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
