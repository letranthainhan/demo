import { Schema } from '@nestjs/mongoose';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { GroupCriteriaDto } from 'src/dto/groupCriteria.dto';
import { GroupCriteria } from 'src/schemas/groupCriteria.schemas';
import { GroupCriteriaService } from './group-criteria.service';

@Controller('group-criteria')
export class GroupCriteriaController {
  constructor(private readonly GroupCriteriaService: GroupCriteriaService) {}
  @Get()
  async findAll(): Promise<GroupCriteria[]> {
    return this.GroupCriteriaService.findAll();
  }
  @Get(':id')
  async findOne(@Param('id') id): Promise<GroupCriteria> {
    return this.GroupCriteriaService.findOne(id);
  }

  @Post()
  create(
    @Body() createGroupCriteriaDto: GroupCriteriaDto,
  ): Promise<GroupCriteria> {
    createGroupCriteriaDto.createAt = this.GroupCriteriaService.At();
    createGroupCriteriaDto.createBy = this.GroupCriteriaService.By();
    return this.GroupCriteriaService.create(createGroupCriteriaDto);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<GroupCriteria> {
    return this.GroupCriteriaService.delete(id);
  }
  @Put(':id')
  update(
    @Body() updateGroupCriteriaDto: GroupCriteriaDto,
    @Param('id') id,
  ): Promise<GroupCriteria> {
    updateGroupCriteriaDto.updateAt = this.GroupCriteriaService.At();
    updateGroupCriteriaDto.updateBy = this.GroupCriteriaService.By();
    return this.GroupCriteriaService.update(id, updateGroupCriteriaDto);
  }
}
