import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from'mongoose';

@Schema()
export class Criteria{
    @Prop({ type: String, required: true })
    name: String;

    @Prop()
    description: String;

    @Prop()
    question: Array<string>;

    @Prop()
    createAt: Date;

    @Prop()
    createBy: String;

    @Prop()
    updateAt: Date;

    @Prop()
    updateBy: String;

    @Prop()
    tags:Array<string>;
}
export const CriteriaSchema=SchemaFactory.createForClass(Criteria);