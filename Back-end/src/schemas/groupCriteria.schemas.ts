import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema()
export class GroupCriteria {
  @Prop({ type: String, required: true })
  name: String;

  @Prop()
  description: String;

  @Prop()
  idCriteria: Array<string>;

  @Prop()
  createAt: Date;

  @Prop()
  createBy: String;

  @Prop()
  updateAt: Date;

  @Prop()
  updateBy: String;
}
export const GroupCriteriaSchema = SchemaFactory.createForClass(GroupCriteria);
