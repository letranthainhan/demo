/** @type {import('tailwindcss').Config} */ 
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      spacing: {
        '100': '31rem',
        '144': '34rem',
        '132': '132px'
   
      }
    },
  },
  plugins: [],
  
}