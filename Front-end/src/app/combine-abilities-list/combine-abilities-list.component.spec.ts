import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CombineAbilitiesListComponent } from './combine-abilities-list.component';

describe('CombineAbilitiesListComponent', () => {
  let component: CombineAbilitiesListComponent;
  let fixture: ComponentFixture<CombineAbilitiesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CombineAbilitiesListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CombineAbilitiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
