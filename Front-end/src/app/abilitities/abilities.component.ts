import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ServicesService } from '../Services/services.service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-abilities',
  templateUrl: './abilities.component.html',
  styleUrls: ['./abilities.component.scss'],
})
export class AbilitiesComponent implements OnInit {
  validateForm!: FormGroup;
  controlArray: Array<{ index: number; show: boolean }> = [];
  listOfControl: Array<{ id: number; controlInstance: string }> = [];

  values: { value: string }[] = [];

  createAbilities(data: any) {
    console.warn(data);
    this.sevices.createTalent(data).subscribe((response) => {
      console.warn(response);
    });
  }

  addField(e?: MouseEvent): void {
    if (e) {
      e.preventDefault();
    }
    const id =
      this.listOfControl.length > 0
        ? this.listOfControl[this.listOfControl.length - 1].id + 1
        : 0;

    const control = {
      id,
      controlInstance: `passenger ${id}`,
    };
    const index = this.listOfControl.push(control);
    console.log(this.listOfControl[this.listOfControl.length - 1]);
    this.validateForm.addControl(
      this.listOfControl[index - 1].controlInstance,
      new FormControl(null, Validators.required)
    );
  }
  removeField(i: { id: number; controlInstance: string }, e: MouseEvent): void {
    e.preventDefault();
    if (this.listOfControl.length > 1) {
      const index = this.listOfControl.indexOf(i);
      this.listOfControl.splice(index, 1);
      console.log(this.listOfControl);
      this.validateForm.removeControl(i.controlInstance);
    }
  }
  submitForm(): void {
    if (this.validateForm.valid) {
      var formData = this.validateForm.value;
      console.log('submit', this.validateForm.value);
      // this.message.success('Thêm thành công');
      // this.sevices.createTalent(formData).subscribe({
      //   next: (value:any) => {
      //     if (value) {
      //       this.router.navigate(['talent', value._id])
      //     }
      //   },
      // })
      this.openAndCloseAll();
      this.validateForm.reset();
    } else {
      Object.values(this.validateForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
  resetForm(): void {
    this.validateForm.reset();
  }

  constructor(
    private fb: FormBuilder,
    private sevices: ServicesService,
    private message: NzMessageService,
    private modal: NzModalService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      abilities: [null, [Validators.required]],
      desc: [null, [Validators.required]],
    });
    this.addField();
  }
  openAndCloseAll(): void {
    let pos = 0;

    ['success'].forEach((method) =>
      // @ts-ignore
      this.modal[method]({
        nzMask: false,
        nzTitle: `Thêm thành công`,
      })
    );

    // this.modal.afterAllClose.subscribe(() => console.log('Thêm bộ khung năng lực thành công'));
    console.log('Thêm bộ khung năng lực thành công');

    // setTimeout(() => this.modal.closeAll(), 2000);
  }

  // removevalue(i) {
  //   this.values.splice(i, 1);
  // }

  // addValue() {
  //   this.values.push({ value: '' });
  // }
}
