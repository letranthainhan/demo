import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGroupabilitiesComponent } from './create-groupabilities.component';

describe('CreateGroupabilitiesComponent', () => {
  let component: CreateGroupabilitiesComponent;
  let fixture: ComponentFixture<CreateGroupabilitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateGroupabilitiesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateGroupabilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
