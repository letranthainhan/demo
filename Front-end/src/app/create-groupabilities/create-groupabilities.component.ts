import { NzMessageService } from 'ng-zorro-antd/message';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-create-groupabilities',
  templateUrl: './create-groupabilities.component.html',
  styleUrls: ['./create-groupabilities.component.scss'],
})
export class CreateGroupabilitiesComponent implements OnInit {
  validateForm!: FormGroup;
  todo = [
    'Chưa có gia đình',
    'Năng động',
    'Sáng tạo',
    'Khả năng làm việc nhóm',
  ];

  done = ['Bằng cấp', 'Kinh nghiệm', 'Tỉ mỉ', 'Tính toán', 'Nhiệt huyết'];

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
  submitForm(): void {
    if (this.validateForm.valid) {
      var formValue = this.validateForm.value;
      formValue['listability'] = this.done
      console.log('submit', formValue);
      this.message.success('Thêm thành công')
      this.validateForm.reset();
    }
    else
    if( formValue === "")
    {
      this.message.error('Thêm thất bại')
      this.validateForm.reset();
    }
     else {
      Object.values(this.validateForm.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
          
        }
      });
    }
  }
  constructor(private fb: FormBuilder, private nzModalRef: NzModalRef,
    private message: NzMessageService ) {}
  ngOnInit(): void {
    this.validateForm = this.fb.group({
      groupAbilitiesName:[null, [Validators.required]]
    });
  }

  closeModal() {
    this.validateForm.reset()
    this.nzModalRef.close({ message: 'Đóng modal' });
    this.message.error('Đóng modal')
  }
  

}
