import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCapcitiesComponent } from './list-capcities.component';

describe('ListCapcitiesComponent', () => {
  let component: ListCapcitiesComponent;
  let fixture: ComponentFixture<ListCapcitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCapcitiesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListCapcitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
