import { CreateGroupabilitiesComponent } from '../create-groupabilities/create-groupabilities.component';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ServicesService } from '../Services/services.service';

@Component({
  selector: 'app-list-abilities',
  templateUrl: './list-abilities.component.html',
  styleUrls: ['./list-abilities.component.scss'],
})
export class ListAbilitiesComponent implements OnInit {
  criterias: any;

  validateForm!: FormGroup;

  log(): void {
    console.log('click dropdown button');
  }
  ngOnInit(): void {}
  constructor(
    private nzModalService: NzModalService,
    private message: NzMessageService,
    private modal: NzModalService,
    private sevices: ServicesService
  ) {
    sevices.getAllTalents().subscribe((data) => {
      console.warn('criterias', data);
      this.criterias = data;
    });
  }
  abilitiesDelete(id: string) {
    this.messageDelete();
    //   this.sevices.deleteTalent(id).subscribe((res)=>{

    // });
  }
  openModal() {
    var modal = this.nzModalService.create({
      nzContent: CreateGroupabilitiesComponent,
      nzClassName: 'create-group-abi-modal',
      nzFooter: null,
    });

    modal.afterClose.subscribe({
      next: (value) => {
        console.log(value);
      },
    });
  }
  messageDelete(): void {
    let pos = 0;

    ['error'].forEach((method) =>
      // @ts-ignore
      this.modal[method]({
        nzMask: false,
        nzTitle: `Bạn có muốn xóa năng lực này?`,
        nzCancelText: 'Cancel',
      })
    );

    // this.modal.afterAllClose.subscribe(() =>  console.log('xóa thành công'));
    console.log('xóa thành công');

    // setTimeout(() => this.modal.closeAll(), 2000);
  }
}
