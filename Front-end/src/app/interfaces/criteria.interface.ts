export interface Criteria {
    id?: string;
    name: string;
    description?: string;
    question?: Array<string>;
    createAt: Date;
    createBy: string;
    updateAt?: Date;
    updateBy?: string;
    tags?:Array<string>;
  }