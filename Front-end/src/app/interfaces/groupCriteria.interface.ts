export interface GroupCriteria {
    id?: string;
    name: string;
    description?: string;
    idCriteria?: Array<string>;
    createAt: Date;
    createBy: string;
    updateAt?: Date;
    updateBy?: string;
  }