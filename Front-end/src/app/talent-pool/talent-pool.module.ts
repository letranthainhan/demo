import { TalentPoolComponent } from './talent-pool.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [TalentPoolComponent],
  imports: [
    CommonModule
  ]
})
export class TalentPoolModule { }
