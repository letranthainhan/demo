import { TalentPoolModule } from './talent-pool/talent-pool.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AbilitiesComponent } from './abilitities/abilities.component';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NzListModule } from 'ng-zorro-antd/list';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListAbilitiesComponent } from './list-abilities/list-abilities.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { CreateGroupabilitiesComponent } from './create-groupabilities/create-groupabilities.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CombineAbilitiesListComponent } from './combine-abilities-list/combine-abilities-list.component';
registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    AbilitiesComponent,
    ListAbilitiesComponent,
    CreateGroupabilitiesComponent,
    CombineAbilitiesListComponent,
  ],
  imports: [
    TalentPoolModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    NzDropDownModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzModalModule,
    NzListModule,
    DragDropModule,
    NzMessageModule,
    ScrollingModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent],
})
export class AppModule {}
