import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BackendservicesService } from '../configures/backendservices.service';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  constructor(private backend: BackendservicesService, private http: HttpClient) {}

  urlPath = this.backend.backendPath + 'criteria';
  

  getAllTalents() {
    return this.http.get(this.urlPath);
  }

  getTalent(id: string) {
    return this.http.get(`${this.urlPath}/${id}`);
  }

  createTalent(data: any) {
    return this.http.post(`${this.urlPath}`, data);
  }

  updateTalent(data: any) {
    return this.http.post(`${this.urlPath}/${data._id}`, data);
  }
  deleteTalent(id: string) {
    return this.http.delete(`${this.urlPath}/${id}`);
  }


}
